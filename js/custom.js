var elms = document.getElementsByClassName("splide");

for (var i = 0; i < elms.length; i++) {
  new Splide(elms[i]).mount();
}

// Wow JS Init
new WOW().init();

// Counter
var counted = 0;

window.addEventListener("scroll", function () {
  var counter = document.getElementById("counter");
  var oTop = counter.offsetTop - window.innerHeight;

  if (counted === 0 && window.pageYOffset > oTop) {
    var countElements = document.getElementsByClassName("count");
    for (var i = 0; i < countElements.length; i++) {
      var countElement = countElements[i];
      var countTo = countElement.getAttribute("data-count");
      animateCount(countElement, countTo);
    }
    counted = 1;
  }
});

function animateCount(element, countTo) {
  var count = parseInt(element.innerText);
  var duration = 2000;
  var startTime = null;

  function step(timestamp) {
    if (!startTime) startTime = timestamp;
    var progress = timestamp - startTime;
    var percentage = Math.min(progress / duration, 1);
    var currentCount = Math.floor(count + percentage * (countTo - count));
    element.innerText = currentCount;

    if (progress < duration) {
      window.requestAnimationFrame(step);
    } else {
      element.innerText = countTo;
      // alert('finished');
    }
  }

  window.requestAnimationFrame(step);
}

// Nav
window.addEventListener("scroll", function () {
  var nav = document.querySelector(".hl-navbar");

  if (window.scrollY >= 100) {
    nav.classList.add("hl-navbar--fixed");
    document.body.classList.add("scrolled");
  } else {
    nav.classList.remove("hl-navbar--fixed");
    document.body.classList.remove("scrolled");
  }
});

// Add Active Class based on URL
(function () {
  var current = location.pathname.split("/")[1];
  if (current === "") return;
  var menuItems = document.querySelectorAll(".nav-item a");
  for (var i = 0, len = menuItems.length; i < len; i++) {
    if (menuItems[i].getAttribute("href").indexOf(current) !== -1) {
      menuItems[i].className += " active";
    }
  }
})();

// Menu
document.addEventListener("DOMContentLoaded", function () {
  var hamburger = document.querySelector(".hl-navbar__hamburger");
  hamburger.addEventListener("click", function () {
    this.classList.toggle("is-active");
  });
});
